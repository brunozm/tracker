package br.com.tracker.infrastructure.auth;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.crypto.password.StandardPasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		
		http
			.csrf().disable()
			.formLogin()
			.loginPage("/login")
			.defaultSuccessUrl("/index", true)
			.permitAll()
			.and()
			.logout().permitAll()
			.and()
			.authorizeRequests()
			.antMatchers("/css/**","/fonts/**","/images/**","/javascript/**").permitAll() 
			.antMatchers("/index/", "/logout").authenticated()
			.antMatchers("/**").hasRole("ADMIN")
			.anyRequest().authenticated();
	}

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth, DataSource dataSource) throws Exception {
		auth.jdbcAuthentication().dataSource(dataSource)
				.usersByUsernameQuery("select name as username, password, true from users where name = ?")
				.authoritiesByUsernameQuery(
						"select  name as username, (CASE WHEN is_admin = true THEN 'ROLE_ADMIN' ELSE 'ROLE_USER' END) from users where name = ?")
				.passwordEncoder(passwordEncoder());
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new StandardPasswordEncoder();
	}
}
