package br.com.tracker.domain;

public class Produto {
	
	private Integer id;
	private String nome;
	private Lote lote;
	
	public Produto(String nome, Lote lote) {
		this.nome = nome;
		this.lote = lote;
	}
	
	public Integer getId() {
		return id;
	}
	
	public String getNome() {
		return nome;
	}

	public Lote getLote() {
		return lote;
	}
	
}
