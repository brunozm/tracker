package br.com.tracker.domain;

import java.util.List;

public class NotaFiscal {
	
	private Integer id;
	private String idTiny;
	private List<QuantidadeProduto> quantidadeProdutos;
	
	public NotaFiscal(String idTiny, List<QuantidadeProduto> quantidadeProdutos) {
		this.idTiny = idTiny;
		this.quantidadeProdutos = quantidadeProdutos;
	}

	public Integer getId() {
		return id;
	}

	public String getIdTiny() {
		return idTiny;
	}

	public List<QuantidadeProduto> getQuantidadeProdutos() {
		return quantidadeProdutos;
	}

}
