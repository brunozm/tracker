package br.com.tracker.domain;

import java.util.Date;

public class Lote {
	
	private Integer id;
	private String nome;
	private Date validade;
	
	public Lote(String nome, Date validade) {
		this.nome = nome;
		this.validade = validade;
	}

	public Integer getId() {
		return id;
	}
	
	public String getNome() {
		return nome;
	}
	
	public Date getValidade() {
		return validade;
	}

}
