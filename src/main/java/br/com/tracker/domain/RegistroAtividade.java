package br.com.tracker.domain;

import java.util.Date;

public class RegistroAtividade {
	
	private Integer id;
	private NotaFiscal notaFiscal;
	private Date data;
	private Atividade atividade;
	
	public RegistroAtividade(NotaFiscal notaFiscal, Date data, Atividade atividade) {
		this.notaFiscal = notaFiscal;
		this.data = data;
		this.atividade = atividade;
	}

	public Integer getId() {
		return id;
	}
	
	public NotaFiscal getNotaFiscal() {
		return notaFiscal;
	}
	
	public Date getData() {
		return data;
	}
	
	public Atividade getAtividade() {
		return atividade;
	}

}
