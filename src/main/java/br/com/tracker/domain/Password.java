package br.com.tracker.domain;

import org.springframework.security.crypto.password.StandardPasswordEncoder;

public class Password {

	private String password;

	public Password(String password) {
		this.password = new StandardPasswordEncoder().encode(password);
	}

	public String getPassword() {
		return password;
	}
	
	public static void main(String[] args) {
		System.out.println(new Password("326598").getPassword());
	}
	
}