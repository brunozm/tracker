package br.com.tracker.domain;

public class QuantidadeProduto {
	
	private Integer id;
	private Produto produto;
	private Integer quantidade;
	
	public QuantidadeProduto(Produto produto, Integer quantidade) {
		this.produto = produto;
		this.quantidade = quantidade;
	}

	public Integer getId() {
		return id;
	}
	
	public Produto getProduto() {
		return produto;
	}
	
	public Integer getQuantidade() {
		return quantidade;
	}
	

}
